#!/usr/bin/perl

my $db=$ARGV[0];
my $list=$ARGV[1];

my %hash;
get_hash($db,\%hash);

my $hold =" \t \t \t \t \t";

open(LIST,"$list")||die "Can't open $list:$!\n";
<LIST>;
while(<LIST>)
{
    chomp;
    chop; #may need to this for ITB2A file but very strang
    my($codon,$chr,$pos,$dummy,$type,$group) = split(/\t/);

    my $line =join("\t",$codon,$chr,$pos,$dummy,$type,$group);
    my($ref,$alt) = split(/\//,$type);

    print STDERR "!!!$ref,$alt,$pos\n";

    if (exists $hash{$pos})
    {
	my %record = %{$hash{$pos}};
	if(exists $record{$alt})
	{
	    my $score = $record{$alt};
	    print STDOUT "$line\t$score\n";

	}
	else
	{
	    print STDERR "something wrong2: $line\n";
	    print STDOUT "$line\n";
	    foreach $key (keys %record)
	    {
		my $score = $record{$key};
		print STDOUT "$hold\t$score\n";
	    }
	}
    }
    else
    {
	print STDERR "something wrong1: $line\n";
    }
}
close(LIST);

sub get_hash($$)
{
    my ($file,$hash_ref) =@_;

    open(DB,"$file") || die "Can't open $file:$!\n";
    while(<DB>)
    {
	chomp;
	my $line =$_;
	my($chr,$pos,$ref,$alt,$aaref,$aaalt,@vals) = split(/\t/);
	$hash_ref->{$pos}{$aaalt} =$line;
    }
    close(DB);
}
