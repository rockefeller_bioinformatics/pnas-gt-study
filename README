boxplots.R
==========

Usage
-----

boxplots.R --args file=FILENAME width=600 height=600 prefix=./

CLI
---

file : The input file.

width (optional) : Output png image width, in pixels. Default value is 600.

height (optional) : Output png image height, in pixels. Default value is 600.

prefix (optional) : The prefix on the output file names. Default value is './'
                    (i.e. the current working directory).

Input File Format
-----------------

 -----------------------------------------------------------------------------------------------------------------------
| Phenotype | val | CADD_raw_rankscore | SIFT_converted_rankscore | Polyphen2_HVAR_rankscore | Polyphen2_HDIV_rankscore |
 -----------------------------------------------------------------------------------------------------------------------
| Alloanti  |  F  |      0.4           |            0.5           |        0.2               |         0.8              |
| Alloanti  |  F  |      0.4           |            0.5           |        0.2               |         0.8              |
| Alloanti  |  F  |      0.4           |            0.5           |        0.2               |         0.8              |

heatmaps.R
==========

Usage
-----

heatmaps.R --args file=FILENAME width=600 height=600 prefix=./

CLI
---

file : The input file.

width (optional) : Output png image width, in pixels. Default value is 600.

height (optional) : Output png image height, in pixels. Default value is 600.

prefix (optional) : The prefix on the output file names. Default value is './'
                    (i.e. the current working directory).

Input File Format
-----------------

 -----------------------------------------------------------------------------------------------------------------
| Phenotype | CADD_raw_rankscore | SIFT_converted_rankscore | Polyphen2_HVAR_rankscore | Polyphen2_HDIV_rankscore |
 -----------------------------------------------------------------------------------------------------------------
| Alloanti  |      0.4           |            0.5           |        0.2               |         0.8              |
| Alloanti  |      0.4           |            0.5           |        0.2               |         0.8              |
| Alloanti  |      0.4           |            0.5           |        0.2               |         0.8              |

pvalue.R
========

Usage
-----

pvalue.R --args file=FILENAME width=3000 height=600 prefix=./

CLI
---

file : The input file.

width (optional) : Output png image width, in pixels. Default value is 3000.

height (optional) : Output png image height, in pixels. Default value is 600.

prefix (optional) : The prefix on the output file names. Default value is './'
                    (i.e. the current working directory).

Input File Format
-----------------

The input file must have the following columns:
 - Phenotype
 - val
 - CADD_raw_rankscore
 - SIFT_converted_rankscore
 - Polyphen2_HVAR_rankscore
 - Polyphen2_HDIV_rankscore
 - Codon
 - Chrom.
 - Position
 - AA.number
 - AA.change
 - LRT_converted_rankscore
 - MutationTaster_converted_rankscore
 - MutationAssessor_rankscore
 - LR_rankscore

roc.R
=====

Usage
-----

roc.R --args files=FILENAME1,FILENAME2 width=600 height=600 prefix=./

CLI
---

files : One or more input files. Multiple files are input with a ',' delimiter.
        Each file must have the same header.

width (optional) : Output png image width, in pixels. Default value is 600.

height (optional) : Output png image height, in pixels. Default value is 600.

prefix (optional) : The prefix on the output file names. Default value is './'
                    (i.e. the current working directory).

Input File Format
-----------------

 --------------------------------------------------------------------------------------
| Phenotype | CADD_raw_rankscore | SIFT_converted_rankscore | Polyphen2_HDIV_rankscore |
 --------------------------------------------------------------------------------------
| Alloanti  |      0.4           |            0.5           |         0.8              |
| Alloanti  |      0.4           |            0.5           |         0.8              |
| Alloanti  |      0.4           |            0.5           |         0.8              |
